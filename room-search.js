// @see http://adventofcode.com/2016/day/4/
const _ = require('lodash');
const inputData = require('./input-data');
function RoomSearch(rooms) {
    const alphabet = 'abcdefghijcklmnopqrstuvwxyz';

    // A room is real (not a decoy) if the checksum is the five most
    // common letters in the encrypted name, in order, with ties broken by alphabetization. For example',

    // aaaaa-bbb-z-y-x-123[abxyz] is a real room because the most common letters are a (5), b (3), and then a tie between x, y, and z, which are listed alphabetically.
    // a-b-c-d-e-f-g-h-987[abcde] is a real room because although the letters are all tied (1 of each), the first five are listed alphabetically.
    // not-a-real-room-404[oarel] is a real room.
    // totally-real-room-200[decoy] is not.
    function isDecoy(room) {
        // In form: [[letter, count]]:
        const letterCount = countLetters(room.encryptedName);
        console.log("Ranked", letterCount);

        // Top 5:
        const rangedAry = letterCount.slice(0, 5);
        console.log("Ranged", rangedAry);

        // Obtain character:
        const top5 = rangedAry.map(el => el[0])
        console.log("TOP5", top5);

        // Join top 5 characters into string:
        const calcChecksum = top5.join('');
        console.log("calcChecksum", calcChecksum);

        // Compare and return:
        console.log('Decoy:', (calcChecksum !== room.checksum), "-> ", calcChecksum,  '!==', room.checksum) ;
        return (calcChecksum !== room.checksum);
    }

    // Count letters in and return an array of arrays:
    function countLetters(str) {
        const letterCount = {}
        const strLen = str.length;

        // Loop through string normally, as clever splitting into
        // char array can break unicode pairs:
        for (let x = 0; x < strLen; ++x) {
            var c = str.charAt(x);
            if (alphabet.indexOf(c) > -1) {
                if (c in letterCount) {
                    ++letterCount[c];
                } else {
                    letterCount[c] = 1;
                }
            }
        }

        // TODO: This could be optimized by storing the
        // item in an array at the proper sorted index
        // rather than sorting after the fact.  See _.sortedIndex
        const letterAry = _.toPairs(letterCount);
        let importance;
        const sortedAry = _.sortBy(letterAry, pair => {
            // Descending sort, with importance placed on first occurrence of char if sum =.
            // This will always be 0.0 <= importance < 1.0, thus will never
            // meet or exceed its calculated sum:
            importance = 1.0 - alphabet.indexOf(pair[0]) / alphabet.length;

            // Store importance for later diagnostics:
            pair[2] = importance;
            return -(pair[1] + importance);
        });

        return sortedAry;
    }

    function sumSectorIds() {
        let sectorSum = 0;
        for (let i=0; i<rooms.length; ++i) {
            room = {
                encryptedName: rooms[i][0],
                sectorId: rooms[i][1],
                checksum: rooms[i][2],
            }
            console.log("Checking room", room)

            if (!isDecoy(room)) {
                sectorSum += parseInt(room.sectorId);
            }
        }
        return sectorSum;
    }

    return sumSectorIds;

}


const sampleInput = [
    ['aaaaa-bbb-z-y-x','123','abxyz'],// is a real room because the most common letters are a (5), b (3), and then a tie between x, y, and z, which are listed alphabetically.
    ['a-b-c-d-e-f-g-h','987','abcde'],// is a real room because although the letters are all tied (1 of each), the first five are listed alphabetically.
    ['not-a-real-room','404','oarel'],// is a real room.
    ['totally-real-room','200','decoy'],// is not.
]

// Create room search closure with test input above:
const inlineDataSearch = RoomSearch(sampleInput);

// Execute:
const sum = inlineDataSearch();

// TODO: Real unit tests would be nice:
console.log("Sector ID sum:", sum, "\nPass:", sum === 123+987+404);


// Create room search closure with included input data from http://adventofcode.com/2016/day/4/
const search = RoomSearch(inputData);

// Execute:
const sectorIdSum = search();

// For challenge input data:
console.log("Sector ID sum:", sectorIdSum, "\nPass:", sectorIdSum === 185371);

// http://adventofcode.com/2016/day/4/answer
// Hurray, we got the right answer.

