## Room search challenge:

### See
http://adventofcode.com/2016/day/4/

### Setup

```bash
git clone git@bitbucket.org:sunder/miers-room-search.git
cd miers-room-search
npm install
npm start
```